<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2011 - Edyta PRZYMUS
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="interp2"><info><pubdate>September 2006</pubdate></info><refnamediv><refname>interp2</refname><refpurpose> a bilinear interplation.</refpurpose></refnamediv>
  
   
  
   
  
   
  
   
  
   
  
   <refsynopsisdiv><title>Calling Sequence</title><synopsis>vp = interp2(xp,yp,x,y,v)</synopsis></refsynopsisdiv>
  
   <refsection><title>Parameters</title>
  
      <variablelist>
    
         <varlistentry>
      
            <term>xp,yp</term>
      
            <listitem>
        : are two real vectors of the same size.
      </listitem>
    
         </varlistentry>
	
         <varlistentry>
		
            <term>x,y</term>
		
            <listitem>
			: strictly increasing row vectors defining the interpolation grid.
		</listitem>
	
         </varlistentry>
	
         <varlistentry>
		
            <term>v</term>
		
            <listitem>
			: is a matrix with the values of the interpolated function at the grid points. 
		</listitem>
    
         </varlistentry>
    
         <varlistentry>
      
            <term>vp</term>
      
            <listitem>
        	: is a matrix containing the returned values of the interpolation.
      </listitem>
    
         </varlistentry>
  
      </variablelist>
  
   </refsection>
  
   <refsection><title>Description</title>
	  
      <para>
		  The function interp2 is a bilinear interpolation, it computes the weighted sum of the function values.
	  </para>
  
   </refsection>
  
   <refsection><title>Examples</title><programlisting role="example"><![CDATA[

n = 8;
x = linspace(0,2*%pi,n); y = x;
z = 2*sin(x')*cos(y);
xx = linspace(0,2*%pi, 40);
[xp,yp] = ndgrid(xx,xx);
zp = interp2(xp,yp, x, y, z);
plot3d(xx, xx, zp, flag=[2 6 4])
[xg,yg] = ndgrid(x,x);
param3d1(xg,yg, list(z,-9*ones(1,n)), flag=[0 0])
xtitle("Bilinear interpolation of 2sin(x)sin(y)")
legends("interpolation points",-9,1)
show_window();
 
  ]]></programlisting></refsection>
   <mediaobject><imageobject><imagedata align="center" fileref="../images/interp2.png"/></imageobject></mediaobject>
  
   <refsection><title>See Also</title><simplelist type="inline">
	  
      <member>
         <link linkend="interp3">interp3</link>
      </member>
  
   </simplelist></refsection>
  
   <para>for more details see <ulink url="http://www.cgal.org/Manual/3.2/doc_html/cgal_manual/Interpolation_ref/Function_linear_interpolation.html">CGAL Manual.</ulink>
   </para>
  
   <para>
  This function uses the Interpolation package of CGAL, which is under QPL license. See <ulink url="http://www.cgal.org/index2.html"> License Terms</ulink>
  
   </para> 
  
   <refsection><title>Authors</title><simplelist type="vert">
    
      <member>Naceur MESKINI.</member>
  
   </simplelist></refsection>

</refentry>
