// ====================================================================
// Edyta PRZYMUS
// Allan CORNET - DIGITEO - 2011
// ====================================================================

mode(-1);
lines(0);

function main_builder()

  TOOLBOX_NAME  = "cglab";
  TOOLBOX_TITLE = "CGLAB";
  toolbox_dir   = get_absolute_file_path("builder.sce");

  // Check Scilab's version
  // =============================================================================

  try
    v = getversion("scilab");
  catch
    error(gettext("Scilab 5.3 or more is required."));
  end
  if v(2) < 3 then
    // new API in scilab 5.3
    error(gettext('Scilab 5.3 or more is required.'));
  end

  // Check modules_manager module availability
  // =============================================================================

  if ~isdef('tbx_build_loader') then
    error(msprintf(gettext('%s module not installed."), 'modules_manager'));
  end

  // Action
  // =============================================================================

  // We check that CGAL is installed
  if getos() == "Windows" then
    CGAL_DIR = getenv('CGAL_DIR', '');
    if CGAL_DIR == '' then
      error(msprintf(gettext('CGAL not detected.")));
    else
      if ~isdir(CGAL_DIR) then
        error(msprintf(gettext('CGAL not detected.")));
      end
    end
  end
  
  tbx_builder_macros(toolbox_dir);
  tbx_builder_src(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

endfunction

main_builder()
clear main_builder
